:orphan:


Exercises - Relativistic Quantum Chemistry (TCCM 2021)
======================================================

These exercises are based on the course given by Trond Saue for
The European Master in Theoretical Chemistry and Computational Modelling (TCCM).

This electronic web material can be part of any workshops on relativistic quantum chemistry,
where theory lessons are coupled with DIRAC computer exercises.

Before embarking on these exercises, it is strongly recommended to review the lectures notes ! Further theoretical background, in particular about Hamiltonians,
is found in Trond Saue, ChemPhysChem **12** (2011) 3077 (`pdf <https://hal.archives-ouvertes.fr/hal-00662643v1>`_).



Pen-and-Paper Exercises
-----------------------

.. toctree::
   :maxdepth: 1

   pen_and_paper.rst


Computer Exercises
------------------

.. toctree::
   :maxdepth: 2

   computer_dirac.rst
